import { useState } from "react"
import logo from './logo.svg';
import './App.css';

function App() {
  const [count, setCount] = useState(0);
  const [name , setName] = useState("");

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        
        <div>
          <h4>Total Click</h4>
          <h1>{count}</h1>
          <button className="App-button" onClick={() => setCount(count + 1)}>+</button>
          <button className="App-button" onClick={() => setCount(count - 1)}>-</button>
        </div>
        <div>
          <h2>Hello, {name}! </h2>
          <button className="App-button" onClick={() => setName("Nilam")}>Nilam</button>
          <button className="App-button" onClick={() => setName("Haekal")}>Haekal</button>
        </div>
      </header>
    </div>
  );
}

export default App;
